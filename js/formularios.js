$(document).ready(function () {

    const formServ = document.querySelector('.formulario');
    //const submitServ = document.querySelector('#enviarServ');

    formServ.addEventListener('submit', function (event) {
        /*var recaptcha = $("#g-recaptcha-response").val();
        if(recaptcha===""){
            event.preventDefault();
            document.getElementById("errorS").textContent = "Por favor, revise el Captcha.";
            return;
        }*/

        // No manda el form
        event.preventDefault();

        // Checar los campos
        const nombre = document.querySelector('.name').value;
        const telefono = document.querySelector('.phone').value;
        const correo = document.querySelector('.email').value;
        const mensaje = document.querySelector('.message').value;
        if (nombre === '' || correo === '' || mensaje === '') {
            document.document.querySelector('.mensaje-error').textContent = "Por favor, llene todos los campos.";
            return;
        }
        else {
            document.document.querySelector('.mensaje-error').textContent = "";
            //this.submit();
            $.ajax({
                type: "POST",
                url: "../php/send_email.php",
                data: $(this).serialize(),
                success: function () {
                    //$('#modalGracias').modal('show');
                }
            });
        }
    });
});